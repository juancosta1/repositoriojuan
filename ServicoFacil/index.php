<!DOCTYPE html>
<html>
<head>
    <?php include_once('meta_tags.html'); ?>
    <title>Serviço fácil - Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php include('navbar.html'); ?>
    
    <div class="container pt-5 justify-content-center">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h1 class="texto">Serviço fácil - Login</h1>

                <form method="post" action="handle_login.php">
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Insira seu e-mail" required>
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" class="form-control" id="senha" name="senha" placeholder="Insira sua senha" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Entrar</button> 
                    <br><br>
                </form>
                <p><a href="cadastro.php?type=cliente">Quero ser cliente do Serviço Fácil</a></p>
                <p><a href="cadastro.php?type=prestador">Sou profissional e quero me candidatar a prestar Serviços</a></p>
            </div>
        </div>
    </div>

    <?php include_once('scripts.html'); ?>
</body>
</html>