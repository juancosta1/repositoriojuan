<?php
  session_start();

  $file_accounts = file_get_contents("accounts.json");
  $accounts = json_decode($file_accounts, true);

  $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
  $senha = filter_input(INPUT_POST, "senha");
  $re_senha = filter_input(INPUT_POST, "re-senha");
  $type = filter_input(INPUT_POST, "type");

  if($senha === $re_senha) {
    $accounts[$email] = array("email" => "not available", "senha" => $senha, "type" => $type);

    $token = base64_encode($email);

    $msg = "Clique no link para confirmar sua conta:\n";
    $msg .= "https://servicofacil.000webhostapp.com/confirmarConta.php?token=" . $token;
    
    mail($email, "Confirme sua Conta no Serviço Fácil", $msg);

    file_put_contents("accounts.json", json_encode($accounts, true));

    header('Location: cadastroConfirmado.php');
    exit;

  } else {
    echo "Confirmação de Senha e Senha devem ser iguais";
    echo "<br>";
    echo "<a href='cadastro.php?type=" . $type . "'><< voltar</a>";
  }