<!DOCTYPE html>
<html>
<head>
    <?php include_once('meta_tags.html'); ?>
    <title>Serviço fácil</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php include('navbar.html'); ?>
    
    <div class="container pt-5 justify-content-center">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h3>Você está cadastrado no Serviço Fácil!</h3>
                <p>Para validar sua conta, você deve acessar o email e clicar no link fornecido.</p>
            </div>
        </div>
    </div>

    <?php include_once('scripts.html'); ?>
</body>
</html>