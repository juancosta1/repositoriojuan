<?php

  $file_accounts = file_get_contents("accounts.json");
  $accounts = json_decode($file_accounts, true);

  $token = filter_input(INPUT_GET, "token", FILTER_SANITIZE_ENCODED);
  $email = base64_decode($token);

  $accounts[$email]["email"] = $email;

  file_put_contents("accounts.json", json_encode($accounts, true));

  echo $token;
  echo '<br>';
  echo $email;
  echo '<br>';
  var_dump($accounts);  