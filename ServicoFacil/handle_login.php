<?php
  session_start();

  $file_accounts = file_get_contents("accounts.json");
  $accounts = json_decode($file_accounts, true);

  $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
  $senha = filter_input(INPUT_POST, "senha");

  if($accounts[$email]) {

    if($accounts[$email]["email"] === 'not available') {
      header('Location: https://servicofacil.000webhostapp.com/contaNaoAtivada.php');
      exit;
    } 
    else {
      if($accounts[$email]["email"] === $email && $accounts[$email]["senha"] === $senha) {
        echo "logged in";
      }
    }
  } else {

    header('Location: https://servicofacil.000webhostapp.com/');
    exit;
  }

  // temporário
  echo '<br>';
  echo $email;
  echo '<br>';
  echo $senha;
  echo '<br>';